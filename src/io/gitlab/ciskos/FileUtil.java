/**
 * 
 */
package io.gitlab.ciskos;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author kot
 *
 */
public class FileUtil {

	public static List<String> getPatcheslist() throws IOException {
		return Files.walk(Paths.get("."))
				.map(f -> f.getFileName().toString())
				.filter(f -> f.endsWith("patch"))
				.collect(Collectors.toList());
	}

	public static void writeGitPatches(Map<String, List<List<String>>> patchesToWrite) throws IOException {
		for (Map.Entry<String, List<List<String>>> patch : patchesToWrite.entrySet()) {
			String key = patch.getKey();
			List<List<String>> value = patch.getValue();
			
			if (Files.notExists(Paths.get("git"))) {
				Files.createDirectory(Paths.get("git"));
			}
			
			FileWriter fw = new FileWriter("git/" + key);
			
			StringBuilder sb = new StringBuilder();
			for (List<String> str : value) {
				for (String s : str) sb.append(s + "\r\n");
			}
			
			fw.write(sb.toString());
			fw.close();
		}
	}
	
}
