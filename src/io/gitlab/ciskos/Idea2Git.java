/**
 * 
 */
package io.gitlab.ciskos;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.gitlab.ciskos.parse.ParseHunk;
import io.gitlab.ciskos.parse.ParsePatch;

/**
 * @author kot
 *
 */
public class Idea2Git {

	private static Map<String, List<List<String>>> patches = new HashMap<>();
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			for (String file : FileUtil.getPatcheslist()) {
				patches.put(file, ParsePatch.patchHunks(file));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for (Map.Entry<String, List<List<String>>> entry : patches.entrySet()) {
			String fileName = entry.getKey();
			List<List<String>> patchesContent = entry.getValue();
			
			for (int i = 0; i < patchesContent.size(); i++) {
				List<String> content = patchesContent.get(i);
				List<String> newContent = ParseHunk.parseHunks(content);

				patchesContent.set(i, newContent);
			}
			
			patches.replace(fileName, patchesContent);
		}
		
		try {
			FileUtil.writeGitPatches(patches);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
