/**
 * 
 */
package io.gitlab.ciskos.parse;

import java.util.List;

/**
 * @author kot
 *
 */
public class ParseHunk {

	public static List<String> parseHunks(List<String> patchLines) {
		String firstLine = patchLines.get(0).split("\t")[0].replaceFirst("--- ", "--- a/");
		String secondLine = patchLines.get(1).split("\t")[0].replaceFirst("\\+\\+\\+ ", "+++ b/");
		String diff = "diff --git " + firstLine.split(" ")[1] + " " + secondLine.split(" ")[1];
		
		patchLines.set(0, firstLine);
		patchLines.set(1, secondLine);
		patchLines.add(0, diff);
		
		return patchLines;
	}
	
}
