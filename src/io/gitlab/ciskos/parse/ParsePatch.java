/**
 * 
 */
package io.gitlab.ciskos.parse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author kot
 *
 */
public class ParsePatch {

	private static String hunksSeparator = "=";
	private static String linesStartsWith = "^[-+@\\\\ ]";

	public static List<List<String>> patchHunks(String file) throws IOException {
		File f = new File(file);
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		List<List<String>> result = new LinkedList<>();
		List<String> hunk = null;

		while (br.ready()) {
			String line = br.readLine();

			if (line.startsWith(hunksSeparator)) {
				if (hunk != null) result.add(hunk);
				hunk = new ArrayList<>();
			} else if (Pattern.compile(linesStartsWith).matcher(line).find()) {
				hunk.add(line);
			}
		}
		
		result.add(hunk);
		br.close();

		return result;
	}

}
